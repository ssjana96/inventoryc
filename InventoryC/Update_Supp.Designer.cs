﻿namespace InventoryC
{
    partial class Update_Supp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Label13 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.TextBox5 = new System.Windows.Forms.TextBox();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.TextBox7 = new System.Windows.Forms.TextBox();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.TextBox8 = new System.Windows.Forms.TextBox();
            this.TextBox9 = new System.Windows.Forms.TextBox();
            this.TextBox10 = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Button4 = new System.Windows.Forms.Button();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Button1 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Panel5.SuspendLayout();
            this.Panel4.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel5.Controls.Add(this.Label13);
            this.Panel5.Controls.Add(this.TextBox3);
            this.Panel5.Controls.Add(this.TextBox4);
            this.Panel5.Controls.Add(this.Label16);
            this.Panel5.Controls.Add(this.Label17);
            this.Panel5.Controls.Add(this.TextBox5);
            this.Panel5.Controls.Add(this.TextBox6);
            this.Panel5.Controls.Add(this.TextBox7);
            this.Panel5.Controls.Add(this.Label18);
            this.Panel5.Controls.Add(this.Label19);
            this.Panel5.Controls.Add(this.TextBox8);
            this.Panel5.Controls.Add(this.TextBox9);
            this.Panel5.Controls.Add(this.TextBox10);
            this.Panel5.Controls.Add(this.Label20);
            this.Panel5.Controls.Add(this.Label21);
            this.Panel5.Controls.Add(this.Label22);
            this.Panel5.Location = new System.Drawing.Point(234, 57);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(536, 358);
            this.Panel5.TabIndex = 38;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(23, 290);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(76, 13);
            this.Label13.TabIndex = 32;
            this.Label13.Text = "Customer Care";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(165, 290);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(143, 20);
            this.TextBox3.TabIndex = 31;
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(165, 257);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Size = new System.Drawing.Size(143, 20);
            this.TextBox4.TabIndex = 29;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(23, 257);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(29, 13);
            this.Label16.TabIndex = 30;
            this.Label16.Text = "PAN";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(23, 229);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(45, 13);
            this.Label17.TabIndex = 29;
            this.Label17.Text = "LUT No";
            // 
            // TextBox5
            // 
            this.TextBox5.Location = new System.Drawing.Point(167, 229);
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Size = new System.Drawing.Size(143, 20);
            this.TextBox5.TabIndex = 28;
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(169, 188);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Size = new System.Drawing.Size(143, 20);
            this.TextBox6.TabIndex = 27;
            // 
            // TextBox7
            // 
            this.TextBox7.Location = new System.Drawing.Point(167, 150);
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Size = new System.Drawing.Size(143, 20);
            this.TextBox7.TabIndex = 26;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(23, 191);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(25, 13);
            this.Label18.TabIndex = 25;
            this.Label18.Text = "CIN";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(23, 157);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(86, 13);
            this.Label19.TabIndex = 24;
            this.Label19.Text = "Enter Phone No.";
            // 
            // TextBox8
            // 
            this.TextBox8.Location = new System.Drawing.Point(165, 47);
            this.TextBox8.Multiline = true;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox8.Size = new System.Drawing.Size(277, 52);
            this.TextBox8.TabIndex = 10;
            // 
            // TextBox9
            // 
            this.TextBox9.Location = new System.Drawing.Point(167, 115);
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Size = new System.Drawing.Size(145, 20);
            this.TextBox9.TabIndex = 9;
            // 
            // TextBox10
            // 
            this.TextBox10.Location = new System.Drawing.Point(165, 10);
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Size = new System.Drawing.Size(277, 20);
            this.TextBox10.TabIndex = 8;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(23, 118);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(49, 13);
            this.Label20.TabIndex = 7;
            this.Label20.Text = "GST_No";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(23, 68);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(45, 13);
            this.Label21.TabIndex = 2;
            this.Label21.Text = "Address";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(23, 12);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(35, 13);
            this.Label22.TabIndex = 0;
            this.Label22.Text = "Name";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(15, 12);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(188, 29);
            this.Label9.TabIndex = 37;
            this.Label9.Text = "Update Supplier";
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.Button4);
            this.Panel4.Controls.Add(this.TextBox1);
            this.Panel4.Controls.Add(this.Label8);
            this.Panel4.Location = new System.Drawing.Point(20, 57);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(208, 82);
            this.Panel4.TabIndex = 36;
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(117, 47);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(75, 23);
            this.Button4.TabIndex = 13;
            this.Button4.Text = "Search";
            this.Button4.UseVisualStyleBackColor = true;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(86, 10);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(117, 20);
            this.TextBox1.TabIndex = 35;
            // 
            // Label8
            // 
            this.Label8.BackColor = System.Drawing.Color.SandyBrown;
            this.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label8.Location = new System.Drawing.Point(4, 13);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(67, 15);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "Supplier ID";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel3.Controls.Add(this.Button1);
            this.Panel3.Controls.Add(this.Button2);
            this.Panel3.Controls.Add(this.Button3);
            this.Panel3.Location = new System.Drawing.Point(234, 430);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(536, 56);
            this.Panel3.TabIndex = 35;
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(239, 17);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(73, 22);
            this.Button1.TabIndex = 12;
            this.Button1.Text = "Cancel";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(25, 15);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(75, 23);
            this.Button2.TabIndex = 10;
            this.Button2.Text = "Save";
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(432, 16);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(75, 23);
            this.Button3.TabIndex = 11;
            this.Button3.Text = "Exit";
            this.Button3.UseVisualStyleBackColor = true;
            // 
            // Update_Supp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 510);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Name = "Update_Supp";
            this.Text = "Update_Supp";
            this.Panel5.ResumeLayout(false);
            this.Panel5.PerformLayout();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.Panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.TextBox TextBox5;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.TextBox TextBox7;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TextBox TextBox8;
        internal System.Windows.Forms.TextBox TextBox9;
        internal System.Windows.Forms.TextBox TextBox10;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button3;
    }
}