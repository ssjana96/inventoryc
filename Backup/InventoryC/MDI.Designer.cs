﻿namespace InventoryC
{
    partial class MDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RegisterSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransactionTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PurchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MakeTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SellToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MakeTransactionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LogOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplierToolStripMenuItem,
            this.CustomerToolStripMenuItem,
            this.TransactionToolStripMenuItem,
            this.LogOutToolStripMenuItem,
            this.ItemsToolStripMenuItem});
            this.statusStrip.Location = new System.Drawing.Point(0, 0);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(821, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            this.statusStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip_ItemClicked);
            // 
            // SupplierToolStripMenuItem
            // 
            this.SupplierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RegisterSupplierToolStripMenuItem,
            this.EditSupplierToolStripMenuItem,
            this.SearchToolStripMenuItem});
            this.SupplierToolStripMenuItem.Name = "SupplierToolStripMenuItem";
            this.SupplierToolStripMenuItem.Size = new System.Drawing.Size(62, 22);
            this.SupplierToolStripMenuItem.Text = "Supplier";
            // 
            // RegisterSupplierToolStripMenuItem
            // 
            this.RegisterSupplierToolStripMenuItem.Name = "RegisterSupplierToolStripMenuItem";
            this.RegisterSupplierToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.RegisterSupplierToolStripMenuItem.Text = "Register Supplier";
            // 
            // EditSupplierToolStripMenuItem
            // 
            this.EditSupplierToolStripMenuItem.Name = "EditSupplierToolStripMenuItem";
            this.EditSupplierToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.EditSupplierToolStripMenuItem.Text = "Edit Supplier";
            // 
            // SearchToolStripMenuItem
            // 
            this.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem";
            this.SearchToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.SearchToolStripMenuItem.Text = "Search";
            // 
            // CustomerToolStripMenuItem
            // 
            this.CustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewCustomerToolStripMenuItem,
            this.SearchCustomerToolStripMenuItem,
            this.EditCustomerToolStripMenuItem});
            this.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem";
            this.CustomerToolStripMenuItem.Size = new System.Drawing.Size(71, 22);
            this.CustomerToolStripMenuItem.Text = "Customer";
            // 
            // NewCustomerToolStripMenuItem
            // 
            this.NewCustomerToolStripMenuItem.Name = "NewCustomerToolStripMenuItem";
            this.NewCustomerToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.NewCustomerToolStripMenuItem.Text = "New Customer";
            // 
            // SearchCustomerToolStripMenuItem
            // 
            this.SearchCustomerToolStripMenuItem.Name = "SearchCustomerToolStripMenuItem";
            this.SearchCustomerToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.SearchCustomerToolStripMenuItem.Text = "Search Customer";
            // 
            // EditCustomerToolStripMenuItem
            // 
            this.EditCustomerToolStripMenuItem.Name = "EditCustomerToolStripMenuItem";
            this.EditCustomerToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.EditCustomerToolStripMenuItem.Text = "Edit Customer";
            // 
            // TransactionToolStripMenuItem
            // 
            this.TransactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransactionTypeToolStripMenuItem,
            this.SearchTransactionToolStripMenuItem});
            this.TransactionToolStripMenuItem.Name = "TransactionToolStripMenuItem";
            this.TransactionToolStripMenuItem.Size = new System.Drawing.Size(81, 22);
            this.TransactionToolStripMenuItem.Text = "Transaction";
            // 
            // TransactionTypeToolStripMenuItem
            // 
            this.TransactionTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PurchaseToolStripMenuItem,
            this.SellToolStripMenuItem});
            this.TransactionTypeToolStripMenuItem.Name = "TransactionTypeToolStripMenuItem";
            this.TransactionTypeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.TransactionTypeToolStripMenuItem.Text = "Transaction Type";
            // 
            // PurchaseToolStripMenuItem
            // 
            this.PurchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MakeTransactionToolStripMenuItem});
            this.PurchaseToolStripMenuItem.Name = "PurchaseToolStripMenuItem";
            this.PurchaseToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.PurchaseToolStripMenuItem.Text = "Purchase";
            // 
            // MakeTransactionToolStripMenuItem
            // 
            this.MakeTransactionToolStripMenuItem.Name = "MakeTransactionToolStripMenuItem";
            this.MakeTransactionToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.MakeTransactionToolStripMenuItem.Text = "Make Transaction";
            // 
            // SellToolStripMenuItem
            // 
            this.SellToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MakeTransactionToolStripMenuItem1});
            this.SellToolStripMenuItem.Name = "SellToolStripMenuItem";
            this.SellToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.SellToolStripMenuItem.Text = "Sell";
            // 
            // MakeTransactionToolStripMenuItem1
            // 
            this.MakeTransactionToolStripMenuItem1.Name = "MakeTransactionToolStripMenuItem1";
            this.MakeTransactionToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.MakeTransactionToolStripMenuItem1.Text = "Make Transaction";
            // 
            // SearchTransactionToolStripMenuItem
            // 
            this.SearchTransactionToolStripMenuItem.Name = "SearchTransactionToolStripMenuItem";
            this.SearchTransactionToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.SearchTransactionToolStripMenuItem.Text = "Search Transaction";
            // 
            // LogOutToolStripMenuItem
            // 
            this.LogOutToolStripMenuItem.Name = "LogOutToolStripMenuItem";
            this.LogOutToolStripMenuItem.Size = new System.Drawing.Size(59, 22);
            this.LogOutToolStripMenuItem.Text = "LogOut";
            // 
            // ItemsToolStripMenuItem
            // 
            this.ItemsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RegisterToolStripMenuItem,
            this.SearchToolStripMenuItem1,
            this.UpdateToolStripMenuItem});
            this.ItemsToolStripMenuItem.Name = "ItemsToolStripMenuItem";
            this.ItemsToolStripMenuItem.Size = new System.Drawing.Size(48, 22);
            this.ItemsToolStripMenuItem.Text = "Items";
            // 
            // RegisterToolStripMenuItem
            // 
            this.RegisterToolStripMenuItem.Name = "RegisterToolStripMenuItem";
            this.RegisterToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.RegisterToolStripMenuItem.Text = "Register";
            // 
            // SearchToolStripMenuItem1
            // 
            this.SearchToolStripMenuItem1.Name = "SearchToolStripMenuItem1";
            this.SearchToolStripMenuItem1.Size = new System.Drawing.Size(116, 22);
            this.SearchToolStripMenuItem1.Text = "Search";
            // 
            // UpdateToolStripMenuItem
            // 
            this.UpdateToolStripMenuItem.Name = "UpdateToolStripMenuItem";
            this.UpdateToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.UpdateToolStripMenuItem.Text = "Update";
            // 
            // MDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 351);
            this.Controls.Add(this.statusStrip);
            this.IsMdiContainer = true;
            this.Name = "MDI";
            this.Text = "MDIParent1";
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolTip toolTip;
        internal System.Windows.Forms.ToolStripMenuItem SupplierToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem RegisterSupplierToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EditSupplierToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SearchToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem CustomerToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem NewCustomerToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SearchCustomerToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EditCustomerToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem TransactionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem TransactionTypeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem PurchaseToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem MakeTransactionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SellToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem MakeTransactionToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem SearchTransactionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem LogOutToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ItemsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem RegisterToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SearchToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem UpdateToolStripMenuItem;
    }
}



