﻿namespace InventoryC
{
    partial class Register_supp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel3 = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.txt_supp = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.txt_lut = new System.Windows.Forms.TextBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.txt_cin = new System.Windows.Forms.TextBox();
            this.txt_gst = new System.Windows.Forms.TextBox();
            this.txt_pan = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.txt_add = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txt_phno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_cust_no = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Panel3.SuspendLayout();
            this.Panel4.SuspendLayout();
            this.Panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel3.Controls.Add(this.btn_cancel);
            this.Panel3.Controls.Add(this.btn_save);
            this.Panel3.Controls.Add(this.btn_exit);
            this.Panel3.Location = new System.Drawing.Point(267, 415);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(579, 56);
            this.Panel3.TabIndex = 41;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(239, 17);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(73, 22);
            this.btn_cancel.TabIndex = 12;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(25, 15);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(432, 16);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 11;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.txt_supp);
            this.Panel4.Controls.Add(this.Label9);
            this.Panel4.Location = new System.Drawing.Point(26, 57);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(208, 41);
            this.Panel4.TabIndex = 40;
            this.Panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel4_Paint);
            // 
            // txt_supp
            // 
            this.txt_supp.Location = new System.Drawing.Point(90, 12);
            this.txt_supp.Name = "txt_supp";
            this.txt_supp.Size = new System.Drawing.Size(93, 20);
            this.txt_supp.TabIndex = 9;
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.Color.SandyBrown;
            this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label9.Location = new System.Drawing.Point(4, 13);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(67, 15);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "Supplier ID";
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel5.Controls.Add(this.txt_cust_no);
            this.Panel5.Controls.Add(this.label3);
            this.Panel5.Controls.Add(this.txt_phno);
            this.Panel5.Controls.Add(this.label2);
            this.Panel5.Controls.Add(this.txt_lut);
            this.Panel5.Controls.Add(this.Label17);
            this.Panel5.Controls.Add(this.Label18);
            this.Panel5.Controls.Add(this.txt_cin);
            this.Panel5.Controls.Add(this.txt_gst);
            this.Panel5.Controls.Add(this.txt_pan);
            this.Panel5.Controls.Add(this.Label19);
            this.Panel5.Controls.Add(this.Label20);
            this.Panel5.Controls.Add(this.txt_add);
            this.Panel5.Controls.Add(this.txt_name);
            this.Panel5.Controls.Add(this.Label21);
            this.Panel5.Controls.Add(this.Label23);
            this.Panel5.Location = new System.Drawing.Point(267, 57);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(579, 352);
            this.Panel5.TabIndex = 39;
            // 
            // txt_lut
            // 
            this.txt_lut.Location = new System.Drawing.Point(165, 257);
            this.txt_lut.Name = "txt_lut";
            this.txt_lut.Size = new System.Drawing.Size(143, 20);
            this.txt_lut.TabIndex = 29;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(23, 257);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(45, 13);
            this.Label17.TabIndex = 30;
            this.Label17.Text = "LUT No";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(23, 229);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(25, 13);
            this.Label18.TabIndex = 29;
            this.Label18.Text = "CIN";
            // 
            // txt_cin
            // 
            this.txt_cin.Location = new System.Drawing.Point(167, 229);
            this.txt_cin.Name = "txt_cin";
            this.txt_cin.Size = new System.Drawing.Size(143, 20);
            this.txt_cin.TabIndex = 28;
            // 
            // txt_gst
            // 
            this.txt_gst.Location = new System.Drawing.Point(169, 188);
            this.txt_gst.Name = "txt_gst";
            this.txt_gst.Size = new System.Drawing.Size(143, 20);
            this.txt_gst.TabIndex = 27;
            // 
            // txt_pan
            // 
            this.txt_pan.Location = new System.Drawing.Point(167, 150);
            this.txt_pan.Name = "txt_pan";
            this.txt_pan.Size = new System.Drawing.Size(143, 20);
            this.txt_pan.TabIndex = 26;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(23, 191);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(49, 13);
            this.Label19.TabIndex = 25;
            this.Label19.Text = "GST_No";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(23, 157);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(29, 13);
            this.Label20.TabIndex = 24;
            this.Label20.Text = "PAN";
            // 
            // txt_add
            // 
            this.txt_add.Location = new System.Drawing.Point(167, 59);
            this.txt_add.Multiline = true;
            this.txt_add.Name = "txt_add";
            this.txt_add.Size = new System.Drawing.Size(275, 50);
            this.txt_add.TabIndex = 9;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(165, 10);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(277, 20);
            this.txt_name.TabIndex = 8;
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(25, 62);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(45, 13);
            this.Label21.TabIndex = 7;
            this.Label21.Text = "Address";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(23, 12);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(35, 13);
            this.Label23.TabIndex = 0;
            this.Label23.Text = "Name";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(26, 13);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label1.Size = new System.Drawing.Size(201, 29);
            this.Label1.TabIndex = 38;
            this.Label1.Text = "Register Supplier";
            // 
            // txt_phno
            // 
            this.txt_phno.Location = new System.Drawing.Point(165, 289);
            this.txt_phno.Name = "txt_phno";
            this.txt_phno.Size = new System.Drawing.Size(143, 20);
            this.txt_phno.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Phone No";
            // 
            // txt_cust_no
            // 
            this.txt_cust_no.Location = new System.Drawing.Point(167, 318);
            this.txt_cust_no.Name = "txt_cust_no";
            this.txt_cust_no.Size = new System.Drawing.Size(143, 20);
            this.txt_cust_no.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 318);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Customer Care";
            // 
            // Register_item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 498);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Label1);
            this.Name = "Register_item";
            this.Text = "Register_supplier";
            this.Load += new System.EventHandler(this.Register_supplier_Load);
            this.Panel3.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.Panel5.ResumeLayout(false);
            this.Panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button btn_cancel;
        internal System.Windows.Forms.Button btn_save;
        internal System.Windows.Forms.Button btn_exit;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.TextBox txt_supp;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.TextBox txt_lut;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TextBox txt_cin;
        internal System.Windows.Forms.TextBox txt_gst;
        internal System.Windows.Forms.TextBox txt_pan;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.TextBox txt_add;
        internal System.Windows.Forms.TextBox txt_name;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txt_cust_no;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txt_phno;
        internal System.Windows.Forms.Label label2;
    }
}