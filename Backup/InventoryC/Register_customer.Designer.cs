﻿namespace InventoryC
{
    partial class Register_customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label2 = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_gst = new System.Windows.Forms.TextBox();
            this.txt_add = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.txt_phno = new System.Windows.Forms.TextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.lbl_cust_id = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.Panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(25, 23);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(214, 29);
            this.Label2.TabIndex = 22;
            this.Label2.Text = "Register Customer";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.lbl_cust_id);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Location = new System.Drawing.Point(25, 71);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(208, 41);
            this.Panel1.TabIndex = 19;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(23, 12);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(35, 13);
            this.Label13.TabIndex = 0;
            this.Label13.Text = "Name";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(23, 68);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(45, 13);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "Address";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(26, 138);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(49, 13);
            this.Label12.TabIndex = 7;
            this.Label12.Text = "GST_No";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(165, 10);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(277, 20);
            this.txt_name.TabIndex = 8;
            // 
            // txt_gst
            // 
            this.txt_gst.Location = new System.Drawing.Point(165, 138);
            this.txt_gst.Name = "txt_gst";
            this.txt_gst.Size = new System.Drawing.Size(145, 20);
            this.txt_gst.TabIndex = 9;
            // 
            // txt_add
            // 
            this.txt_add.Location = new System.Drawing.Point(165, 68);
            this.txt_add.Multiline = true;
            this.txt_add.Name = "txt_add";
            this.txt_add.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_add.Size = new System.Drawing.Size(277, 58);
            this.txt_add.TabIndex = 10;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(26, 169);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(86, 13);
            this.Label8.TabIndex = 24;
            this.Label8.Text = "Enter Phone No.";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(29, 210);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(60, 13);
            this.Label9.TabIndex = 25;
            this.Label9.Text = "Enter Email";
            // 
            // txt_phno
            // 
            this.txt_phno.Location = new System.Drawing.Point(166, 169);
            this.txt_phno.Name = "txt_phno";
            this.txt_phno.Size = new System.Drawing.Size(143, 20);
            this.txt_phno.TabIndex = 26;
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(167, 210);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(143, 20);
            this.txt_email.TabIndex = 27;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel3.Controls.Add(this.txt_email);
            this.Panel3.Controls.Add(this.txt_phno);
            this.Panel3.Controls.Add(this.Label9);
            this.Panel3.Controls.Add(this.Label8);
            this.Panel3.Controls.Add(this.txt_add);
            this.Panel3.Controls.Add(this.txt_gst);
            this.Panel3.Controls.Add(this.txt_name);
            this.Panel3.Controls.Add(this.Label12);
            this.Panel3.Controls.Add(this.Label7);
            this.Panel3.Controls.Add(this.Label13);
            this.Panel3.Location = new System.Drawing.Point(234, 71);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(491, 252);
            this.Panel3.TabIndex = 20;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(385, 16);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 11;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(25, 15);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(199, 16);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(73, 22);
            this.btn_cancel.TabIndex = 12;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel6.Controls.Add(this.btn_cancel);
            this.Panel6.Controls.Add(this.btn_save);
            this.Panel6.Controls.Add(this.btn_exit);
            this.Panel6.Location = new System.Drawing.Point(235, 329);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(491, 56);
            this.Panel6.TabIndex = 21;
            // 
            // lbl_cust_id
            // 
            this.lbl_cust_id.BackColor = System.Drawing.Color.NavajoWhite;
            this.lbl_cust_id.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_cust_id.Location = new System.Drawing.Point(70, 13);
            this.lbl_cust_id.Name = "lbl_cust_id";
            this.lbl_cust_id.Size = new System.Drawing.Size(100, 15);
            this.lbl_cust_id.TabIndex = 1;
            this.lbl_cust_id.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.SandyBrown;
            this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label1.Location = new System.Drawing.Point(22, 13);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 15);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Cust Id";
            // 
            // Register_customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 461);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Panel6);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel1);
            this.Name = "Register_customer";
            this.Text = "Register_Supplier";
            this.Panel1.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            this.Panel3.PerformLayout();
            this.Panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox txt_name;
        internal System.Windows.Forms.TextBox txt_gst;
        internal System.Windows.Forms.TextBox txt_add;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox txt_phno;
        internal System.Windows.Forms.TextBox txt_email;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button btn_exit;
        internal System.Windows.Forms.Button btn_save;
        internal System.Windows.Forms.Button btn_cancel;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.Label lbl_cust_id;
        internal System.Windows.Forms.Label Label1;
    }
}