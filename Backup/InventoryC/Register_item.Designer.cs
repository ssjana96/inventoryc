﻿namespace InventoryC
{
    partial class Register_item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel3 = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.txt_supp = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Label2 = new System.Windows.Forms.Label();
            this.combo_unit = new System.Windows.Forms.ComboBox();
            this.txt_oum = new System.Windows.Forms.TextBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.txt_hsn = new System.Windows.Forms.TextBox();
            this.txtx_sgstpct = new System.Windows.Forms.TextBox();
            this.txt_cgstpct = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.txt_rate = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Panel3.SuspendLayout();
            this.Panel4.SuspendLayout();
            this.Panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel3.Controls.Add(this.btn_cancel);
            this.Panel3.Controls.Add(this.btn_save);
            this.Panel3.Controls.Add(this.btn_exit);
            this.Panel3.Location = new System.Drawing.Point(272, 407);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(579, 56);
            this.Panel3.TabIndex = 41;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(239, 17);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(73, 22);
            this.btn_cancel.TabIndex = 12;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(25, 15);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(432, 16);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 11;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.txt_supp);
            this.Panel4.Controls.Add(this.Label9);
            this.Panel4.Location = new System.Drawing.Point(31, 81);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(208, 41);
            this.Panel4.TabIndex = 40;
            // 
            // txt_supp
            // 
            this.txt_supp.Location = new System.Drawing.Point(90, 12);
            this.txt_supp.Name = "txt_supp";
            this.txt_supp.Size = new System.Drawing.Size(93, 20);
            this.txt_supp.TabIndex = 9;
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.Color.SandyBrown;
            this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label9.Location = new System.Drawing.Point(4, 13);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(67, 15);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "Item ID";
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel5.Controls.Add(this.Label2);
            this.Panel5.Controls.Add(this.combo_unit);
            this.Panel5.Controls.Add(this.txt_oum);
            this.Panel5.Controls.Add(this.Label17);
            this.Panel5.Controls.Add(this.Label18);
            this.Panel5.Controls.Add(this.txt_hsn);
            this.Panel5.Controls.Add(this.txtx_sgstpct);
            this.Panel5.Controls.Add(this.txt_cgstpct);
            this.Panel5.Controls.Add(this.Label19);
            this.Panel5.Controls.Add(this.Label20);
            this.Panel5.Controls.Add(this.txt_rate);
            this.Panel5.Controls.Add(this.txt_name);
            this.Panel5.Controls.Add(this.Label21);
            this.Panel5.Controls.Add(this.Label23);
            this.Panel5.Location = new System.Drawing.Point(272, 81);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(579, 320);
            this.Panel5.TabIndex = 39;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(23, 70);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(26, 13);
            this.Label2.TabIndex = 36;
            this.Label2.Text = "Unit";
            // 
            // combo_unit
            // 
            this.combo_unit.FormattingEnabled = true;
            this.combo_unit.Location = new System.Drawing.Point(169, 62);
            this.combo_unit.Name = "combo_unit";
            this.combo_unit.Size = new System.Drawing.Size(139, 21);
            this.combo_unit.TabIndex = 35;
            // 
            // txt_oum
            // 
            this.txt_oum.Location = new System.Drawing.Point(165, 257);
            this.txt_oum.Name = "txt_oum";
            this.txt_oum.Size = new System.Drawing.Size(143, 20);
            this.txt_oum.TabIndex = 29;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(23, 257);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(32, 13);
            this.Label17.TabIndex = 30;
            this.Label17.Text = "OUM";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(23, 229);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(30, 13);
            this.Label18.TabIndex = 29;
            this.Label18.Text = "HSN";
            // 
            // txt_hsn
            // 
            this.txt_hsn.Location = new System.Drawing.Point(167, 229);
            this.txt_hsn.Name = "txt_hsn";
            this.txt_hsn.Size = new System.Drawing.Size(143, 20);
            this.txt_hsn.TabIndex = 28;
            // 
            // txtx_sgstpct
            // 
            this.txtx_sgstpct.Location = new System.Drawing.Point(169, 188);
            this.txtx_sgstpct.Name = "txtx_sgstpct";
            this.txtx_sgstpct.Size = new System.Drawing.Size(143, 20);
            this.txtx_sgstpct.TabIndex = 27;
            // 
            // txt_cgstpct
            // 
            this.txt_cgstpct.Location = new System.Drawing.Point(167, 150);
            this.txt_cgstpct.Name = "txt_cgstpct";
            this.txt_cgstpct.Size = new System.Drawing.Size(143, 20);
            this.txt_cgstpct.TabIndex = 26;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(23, 191);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(47, 13);
            this.Label19.TabIndex = 25;
            this.Label19.Text = "SGST %";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(23, 157);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(47, 13);
            this.Label20.TabIndex = 24;
            this.Label20.Text = "CGST %";
            // 
            // txt_rate
            // 
            this.txt_rate.Location = new System.Drawing.Point(167, 115);
            this.txt_rate.Name = "txt_rate";
            this.txt_rate.Size = new System.Drawing.Size(145, 20);
            this.txt_rate.TabIndex = 9;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(165, 10);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(277, 20);
            this.txt_name.TabIndex = 8;
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(23, 118);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(30, 13);
            this.Label21.TabIndex = 7;
            this.Label21.Text = "Rate";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(23, 12);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(35, 13);
            this.Label23.TabIndex = 0;
            this.Label23.Text = "Name";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(31, 32);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label1.Size = new System.Drawing.Size(156, 29);
            this.Label1.TabIndex = 38;
            this.Label1.Text = "Register Item";
            // 
            // Register_item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 500);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Label1);
            this.Name = "Register_item";
            this.Text = "Register_item";
            this.Panel3.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.Panel5.ResumeLayout(false);
            this.Panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Button btn_cancel;
        internal System.Windows.Forms.Button btn_save;
        internal System.Windows.Forms.Button btn_exit;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.TextBox txt_supp;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox combo_unit;
        internal System.Windows.Forms.TextBox txt_oum;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TextBox txt_hsn;
        internal System.Windows.Forms.TextBox txtx_sgstpct;
        internal System.Windows.Forms.TextBox txt_cgstpct;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.TextBox txt_rate;
        internal System.Windows.Forms.TextBox txt_name;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label1;
    }
}